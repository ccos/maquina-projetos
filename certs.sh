#!/usr/bin/env bash

# Criar ou renovar certificados pelo Let's Encrypt
docker run -v letsencrypt:/etc/letsencrypt -p 80:80 certbot/certbot:v1.22.0 \
    certonly -n --standalone --agree-tos \
    -m webmaster@ccos.icmc.usp.br \
    -d ccsl.icmc.usp.br \
    -d napsol.icmc.usp.br \
    -d qualipso.icmc.usp.br \
